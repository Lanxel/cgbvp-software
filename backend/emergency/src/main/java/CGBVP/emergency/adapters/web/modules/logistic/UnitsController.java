package CGBVP.emergency.adapters.web.modules.logistic;

import CGBVP.emergency.commons.hexagonal.WebAdapter;
import CGBVP.emergency.intranet.emergency.domain.Emergency;
import CGBVP.emergency.intranet.logistic.application.port.in.GetUnitListUseCase;
import CGBVP.emergency.intranet.logistic.application.port.in.RegisterUnitUseCase;
import CGBVP.emergency.intranet.logistic.application.port.in.UnitToRegister;
import CGBVP.emergency.intranet.logistic.domain.Unit;
import CGBVP.emergency.intranet.session_manager.application.port.in.GetUsersListUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@WebAdapter
@RestController
@RequiredArgsConstructor
@CrossOrigin(value = "*")
public class UnitsController {
    private final RegisterUnitUseCase registerUnitUseCase;
    private final GetUnitListUseCase getUnitListUseCase;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "/units/create")
    public Unit createUnit(@RequestBody UnitToRegister unitToRegister) {
        return registerUnitUseCase.execute(unitToRegister);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/unit/list/{code}")
    public List<Unit> getUnitListByCode(@PathVariable String code) {
        return getUnitListUseCase.execute(code);
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/unit/list")
    public List<Unit> getUnitList() {
        return getUnitListUseCase.execute();
    }

}
