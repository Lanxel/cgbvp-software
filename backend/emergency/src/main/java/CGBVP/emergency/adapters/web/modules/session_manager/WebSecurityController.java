package CGBVP.emergency.adapters.web.modules.session_manager;

import CGBVP.emergency.adapters.web.modules.session_manager.config.security.LoginService;
import CGBVP.emergency.commons.hexagonal.WebAdapter;
import CGBVP.emergency.intranet.session_manager.domain.Credentials;
import CGBVP.emergency.intranet.session_manager.domain.User;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@RestController
@RequiredArgsConstructor
@CrossOrigin(value = "*")
public class WebSecurityController {
    private final LoginService loginService;

    @PostMapping("/auth/login")
    @ResponseStatus(HttpStatus.OK)
    public User login(@RequestBody Credentials credentials) {
        return loginService.authUser(credentials);
    }

}
