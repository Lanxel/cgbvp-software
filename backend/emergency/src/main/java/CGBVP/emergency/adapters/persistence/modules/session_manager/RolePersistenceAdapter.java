package CGBVP.emergency.adapters.persistence.modules.session_manager;

import CGBVP.emergency.commons.hexagonal.PersistenceAdapter;
import CGBVP.emergency.intranet.session_manager.application.port.out.GetRoleListPort;
import CGBVP.emergency.intranet.session_manager.domain.Role;
import lombok.RequiredArgsConstructor;

import java.util.List;

@PersistenceAdapter
@RequiredArgsConstructor
public class RolePersistenceAdapter implements GetRoleListPort {
    private final RoleModelRepository repository;
    private final RoleMapper mapper;
    @Override
    public List<Role> getList() {
        return mapper.toRoleList(repository.findAll());
    }
}
