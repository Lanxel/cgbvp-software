package CGBVP.emergency.adapters.web.modules.session_manager.config.security;

import CGBVP.emergency.adapters.persistence.modules.session_manager.AccountModelRepository;
import CGBVP.emergency.adapters.persistence.modules.session_manager.UserMapper;
import CGBVP.emergency.adapters.persistence.modules.session_manager.UserModelRepository;
import CGBVP.emergency.intranet.session_manager.domain.Credentials;
import CGBVP.emergency.intranet.session_manager.domain.User;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service("Login")
@RequiredArgsConstructor
public class LoginService {
    private final AccountModelRepository accountModelRepository;
    private final UserModelRepository userModelRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserMapper mapper;

    public User authUser(Credentials  credentials){
        var account = accountModelRepository.findByUsername(credentials.getUsername())
                .orElseThrow(() -> new RuntimeException("Username Not Found"));
        var user = userModelRepository.findById(account.getUserId()).orElseThrow(() -> new RuntimeException("User Not Found"));
        if (passwordEncoder.matches(credentials.getPassword(),account.getPassword())){
            return mapper.toUser(user);
        }
        throw new RuntimeException("No se ha podido autenticar");
    }
}
