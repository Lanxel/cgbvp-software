package CGBVP.emergency.adapters.persistence.modules.transcriber;

import CGBVP.emergency.commons.hexagonal.PersistenceAdapter;
import CGBVP.emergency.intranet.transcriber.application.port.out.GetDefaultTextListPort;
import CGBVP.emergency.intranet.transcriber.domain.DefaultText;
import lombok.RequiredArgsConstructor;

import java.util.List;

@PersistenceAdapter
@RequiredArgsConstructor
public class DefaultTextPersistenceAdapter implements GetDefaultTextListPort {
    private final DefaultTextModelRepository repository;
    private final DefaultTextMapper mapper;
    @Override
    public List<DefaultText> getList(String text) {
        return mapper.toDefaulTextList(repository.findAllDefaultTextByText(text));
    }

    @Override
    public List<DefaultText> getList() {
        return mapper.toDefaulTextList(repository.findAllDefaultTextByText(""));
    }
}
