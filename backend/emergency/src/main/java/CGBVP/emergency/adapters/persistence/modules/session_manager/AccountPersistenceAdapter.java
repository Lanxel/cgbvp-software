package CGBVP.emergency.adapters.persistence.modules.session_manager;

import CGBVP.emergency.commons.hexagonal.PersistenceAdapter;
import CGBVP.emergency.intranet.session_manager.application.port.in.AccountToRegister;
import CGBVP.emergency.intranet.session_manager.application.port.out.RegisterAccountPort;
import CGBVP.emergency.intranet.session_manager.domain.Account;
import lombok.RequiredArgsConstructor;

@PersistenceAdapter
@RequiredArgsConstructor
public class AccountPersistenceAdapter implements RegisterAccountPort {
    private final AccountModelRepository repository;
    private final AccountMapper mapper;

    @Override
    public Account register(AccountToRegister accountToRegister) {
        var row = mapper.toModel(accountToRegister);
        return mapper.toAccount(repository.save(row));
    }
}
