package CGBVP.emergency.adapters.persistence.modules.emergency;

import CGBVP.emergency.adapters.persistence.modules.transcriber.DefaultTextMapper;
import CGBVP.emergency.intranet.emergency.domain.EmergencyType;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = {EmergencySubTypeMapper.class})
public interface EmergencyTypeMapper {
    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    @Mapping(target = "code", source = "code")
    @Mapping(target = "subTypeList", source = "emergencySubTypeList")
    EmergencyType toEmergencyType(EmergencyTypeModel model);
    List<EmergencyType> toEmergencyTypeList(List<EmergencyTypeModel> emergencyTypeModels);
}
