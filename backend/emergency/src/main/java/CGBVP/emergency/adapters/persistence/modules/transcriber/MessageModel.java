package CGBVP.emergency.adapters.persistence.modules.transcriber;

import CGBVP.emergency.adapters.persistence.modules.emergency.EmergencyModel;
import CGBVP.emergency.adapters.persistence.modules.session_manager.UserModel;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "MENSAJE")
public class MessageModel {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "mensaje_id")
    private Long id;

    @Column(name = "codigo",nullable = false)
    private String code;

    @Column(name = "tipo",nullable = false)
    private String type;

    @Column(name = "eliminado", nullable = false)
    private boolean deleted = false;

    @Column(name = "contenido_id")
    private Long contentId;

    @Column(name = "usuario_id")
    private Long userId;

    @Column(name = "emergencia_id")
    private Long emergencyId;

    @CreatedDate
    @CreationTimestamp
    @ColumnDefault("CURRENT_TIMESTAMP")
    @Column(name = "fecha_creacion")
    private Timestamp createdAt;

    @LastModifiedDate
    @UpdateTimestamp
    @ColumnDefault("CURRENT_TIMESTAMP")
    @Column(name = "fecha_modificacion")
    private Timestamp updatedAt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario_id",insertable = false, updatable = false)
    private UserModel user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "emergencia_id",insertable = false, updatable = false)
    private EmergencyModel emergency;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "contenido_id",insertable = false, updatable = false)
    private ContentModel content;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        MessageModel message = (MessageModel) o;
        return id != null && Objects.equals(id, message.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
