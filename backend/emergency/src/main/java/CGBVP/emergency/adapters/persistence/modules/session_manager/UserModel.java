package CGBVP.emergency.adapters.persistence.modules.session_manager;

import CGBVP.emergency.adapters.persistence.modules.emergency.FireCompanyModel;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "USUARIO")
public class UserModel {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "usuario_id")
    private Long id;

    @Column(name = "nombres", nullable = false)
    private String name;

    @Column(name = "apellidos", nullable = false)
    private String lastname;

    @Column(name = "correo",nullable = false)
    private String email;

    @Column(name = "numero_contacto")
    private String contactNumber;

    @Column(name = "genero", nullable = false)
    private String gender;

    @Column(name = "documento", length = 8, nullable = false)
    private String dni;

    @Column(name = "tipo_sangre",length = 3 ,nullable = false)
    private String bloodType;

    @Column(name = "cargo", nullable = false)
    private String position;

    @Column(name = "activo", nullable = false)
    private boolean active = true;

    @Column(name = "fecha_nacimiento",nullable = false)
    private Date dateBirth;

    @Column(name = "rol_id",nullable = false)
    private Long roleId;

    @Column(name = "compania_bomberos_id")
    private Long companyId;

    @CreatedDate
    @CreationTimestamp
    @ColumnDefault("CURRENT_TIMESTAMP")
    @Column(name = "fecha_creacion")
    private Timestamp createdAt;

    @LastModifiedDate
    @UpdateTimestamp
    @ColumnDefault("CURRENT_TIMESTAMP")
    @Column(name = "fecha_modificacion")
    private Timestamp updatedAt;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "rol_id",insertable = false, updatable = false)
    private RoleModel role;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "compania_bomberos_id",insertable = false, updatable = false)
    private FireCompanyModel company;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        UserModel user = (UserModel) o;
        return id != null && Objects.equals(id, user.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
