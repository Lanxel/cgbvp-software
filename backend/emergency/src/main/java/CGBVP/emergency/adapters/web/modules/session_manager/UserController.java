package CGBVP.emergency.adapters.web.modules.session_manager;

import CGBVP.emergency.commons.hexagonal.WebAdapter;
import CGBVP.emergency.intranet.session_manager.application.port.in.*;
import CGBVP.emergency.intranet.session_manager.domain.User;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@WebAdapter
@RestController
@RequiredArgsConstructor
@CrossOrigin(value = "*")
public class UserController {
    private final RegisterUserUseCase registerUserUseCase;
    private final UpdateUserUseCase updateUserUseCase;
    private final GetUsersListUseCase getUsersListUseCase;
    private final GetUserUseCase getUserUseCase;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "/user/create")
    public User createUser(@RequestBody UserToRegister userToRegister) {
        return registerUserUseCase.execute(userToRegister);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping(value = "/user/update")
    public User updateUser(@RequestBody UserToRegister userToRegister, Long userId) {
        return updateUserUseCase.execute(userToRegister, userId);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/user/list")
    public List<User> getUsersList() {
        return getUsersListUseCase.execute();
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/user/list/{name}")
    public List<User> getUsersList(@PathVariable String name) {
        return getUsersListUseCase.execute(name);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/user/{userId}")
    public User getUserById(@PathVariable Long userId) {
        return getUserUseCase.execute(userId);
    }


}
