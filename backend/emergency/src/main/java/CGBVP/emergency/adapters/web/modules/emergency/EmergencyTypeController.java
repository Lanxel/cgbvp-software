package CGBVP.emergency.adapters.web.modules.emergency;

import CGBVP.emergency.commons.hexagonal.WebAdapter;
import CGBVP.emergency.intranet.emergency.application.port.in.GetEmergencyTypeListUseCase;
import CGBVP.emergency.intranet.emergency.domain.EmergencyType;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@WebAdapter
@RestController
@RequiredArgsConstructor
@CrossOrigin(value = "*")
public class EmergencyTypeController {
    private final GetEmergencyTypeListUseCase getEmergencyTypeListUseCase;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/emergencyType/list")
    public List<EmergencyType> getEmergencyList() {
        return getEmergencyTypeListUseCase.execute();
    }
}
