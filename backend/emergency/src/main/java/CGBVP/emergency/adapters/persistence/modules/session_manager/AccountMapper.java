package CGBVP.emergency.adapters.persistence.modules.session_manager;

import CGBVP.emergency.intranet.session_manager.application.port.in.AccountToRegister;
import CGBVP.emergency.intranet.session_manager.domain.Account;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring",uses = {UserMapper.class})
public interface AccountMapper {
    @Mapping(target = "id", source = "id")
    @Mapping(target = "type", source = "type")
    @Mapping(target = "domain", source = "domain")
    @Mapping(target = "imagePath", source = "imagePath")
    @Mapping(target = "user", source = "user")
    @Mapping(target = "username", source = "username")
    @Mapping(target = "password", source = "password")
    Account toAccount(AccountModel model);

    @Mapping(target = "userId", source = "userId")
    @Mapping(target = "domain", source = "domain")
    @Mapping(target = "imagePath", source = "imagePath")
    @Mapping(target = "type", source = "type")
    @Mapping(target = "username", source = "username")
    @Mapping(target = "password", source = "password")
    AccountModel toModel(AccountToRegister userToRegister);
}
