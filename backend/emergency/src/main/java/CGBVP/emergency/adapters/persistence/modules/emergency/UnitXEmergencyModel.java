package CGBVP.emergency.adapters.persistence.modules.emergency;

import CGBVP.emergency.adapters.persistence.modules.logistic.UnitModel;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "UNIDAD_X_EMERGENCIA")
public class UnitXEmergencyModel {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "unidad_x_emergencia_id")
    private Long id;

    @Column(name = "unidad_id", nullable = false)
    private Long unitId;

    @Column(name = "emergencia_id", nullable = false)
    private Long emergencyId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "unidad_id", insertable = false, updatable = false)
    private UnitModel unit;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "emergencia_id", insertable = false, updatable = false)
    private EmergencyModel emergency;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        UnitXEmergencyModel that = (UnitXEmergencyModel) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
