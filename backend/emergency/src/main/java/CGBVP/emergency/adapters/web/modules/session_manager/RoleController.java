package CGBVP.emergency.adapters.web.modules.session_manager;

import CGBVP.emergency.adapters.persistence.modules.session_manager.RoleMapper;
import CGBVP.emergency.commons.hexagonal.WebAdapter;
import CGBVP.emergency.intranet.session_manager.application.port.in.GetRoleListUseCase;
import CGBVP.emergency.intranet.session_manager.domain.Role;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@WebAdapter
@RestController
@RequiredArgsConstructor
@CrossOrigin(value = "*")
public class RoleController {
    private final GetRoleListUseCase getRoleListUseCase;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/role/list")
    public List<Role> getRoleList() {
        return getRoleListUseCase.execute();
    }
}
