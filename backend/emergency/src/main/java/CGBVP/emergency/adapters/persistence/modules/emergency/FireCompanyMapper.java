package CGBVP.emergency.adapters.persistence.modules.emergency;

import CGBVP.emergency.intranet.emergency.domain.FireCompany;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;
@Mapper(componentModel = "spring")
public interface FireCompanyMapper {
    @Mapping(target = "id", source = "id")
    @Mapping(target = "address", source = "address")
    @Mapping(target = "district", source = "district")
    @Mapping(target = "manager", source = "manager")
    @Mapping(target = "name", source = "name")
    @Mapping(target = "state", source = "state")
    FireCompany toFireCompany(FireCompanyModel model);
    List<FireCompany> toFireCompanyList(List<FireCompanyModel> emergencyModels);

    @Mapping(target = "district", source = "district")
    @Mapping(target = "address", source = "address")
    @Mapping(target = "name", source = "name")
    @Mapping(target = "state", source = "state")
    @Mapping(target = "manager", source = "manager")
    @Mapping(target = "id", source = "id")
    FireCompanyModel toModel(FireCompany toEmergency);
}
