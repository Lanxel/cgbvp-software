package CGBVP.emergency.adapters.persistence.modules.session_manager;

import CGBVP.emergency.adapters.persistence.modules.emergency.FireCompanyMapper;
import CGBVP.emergency.intranet.session_manager.application.port.in.UserToRegister;
import CGBVP.emergency.intranet.session_manager.domain.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = {RoleMapper.class, FireCompanyMapper.class})
public interface UserMapper {
    @Mapping(target = "id", source = "id")
    @Mapping(target = "dateBirth", source = "dateBirth")
    @Mapping(target = "dni", source = "dni")
    @Mapping(target = "name", source = "name")
    @Mapping(target = "lastname", source = "lastname")
    @Mapping(target = "gender", source = "gender")
    @Mapping(target = "bloodType", source = "bloodType")
    @Mapping(target = "contactNumber", source = "contactNumber")
    @Mapping(target = "createdAt", source = "createdAt")
    @Mapping(target = "updatedAt", source = "updatedAt")
    @Mapping(target = "role", source = "role.name")
    @Mapping(target = "active", source = "active")
    @Mapping(target = "email", source = "email")
    @Mapping(target = "position", source = "position")
    @Mapping(target = "company", source = "company")
    User toUser(UserModel model);
    List<User> toUserList(List<UserModel> emergencyModels);

    @Mapping(target = "roleId", source = "roleId")
    @Mapping(target = "dni", source = "dni")
    @Mapping(target = "position", source = "position")
    @Mapping(target = "bloodType", source = "bloodType")
    @Mapping(target = "email", source = "email")
    @Mapping(target = "contactNumber", source = "contactNumber")
    @Mapping(target = "gender", source = "gender")
    @Mapping(target = "name", source = "name")
    @Mapping(target = "lastname", source = "lastname")
    @Mapping(target = "dateBirth", source = "dateBirth")
    @Mapping(target = "companyId", source = "companyId")
    UserModel toModel(UserToRegister userToRegister);
}
