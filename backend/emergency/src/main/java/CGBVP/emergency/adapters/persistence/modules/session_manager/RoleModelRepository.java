package CGBVP.emergency.adapters.persistence.modules.session_manager;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleModelRepository extends JpaRepository<RoleModel, Long> {
}