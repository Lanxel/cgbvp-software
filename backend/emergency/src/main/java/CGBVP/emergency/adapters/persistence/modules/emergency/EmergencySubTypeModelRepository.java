package CGBVP.emergency.adapters.persistence.modules.emergency;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EmergencySubTypeModelRepository extends JpaRepository<EmergencySubTypeModel, Long> {
}