package CGBVP.emergency.adapters.persistence.modules.session_manager;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AccountModelRepository extends JpaRepository<AccountModel, Long> {
    Optional<AccountModel> findByUsername(String username);
}