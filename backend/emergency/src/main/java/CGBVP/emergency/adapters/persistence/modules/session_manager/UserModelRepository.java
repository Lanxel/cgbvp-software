package CGBVP.emergency.adapters.persistence.modules.session_manager;

import CGBVP.emergency.adapters.persistence.modules.transcriber.MessageModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserModelRepository extends JpaRepository<UserModel, Long> {
    @Query(value= "SELECT * FROM usuario where activo is true and nombres ilike(concat('%',?1,'%'))  order by fecha_creacion desc", nativeQuery = true)
    List<UserModel> findAllActiveUsersOrdered(String name);
}