package CGBVP.emergency.intranet.session_manager.application.port;

import CGBVP.emergency.commons.hexagonal.UseCase;
import CGBVP.emergency.intranet.session_manager.application.port.in.GetRoleListUseCase;
import CGBVP.emergency.intranet.session_manager.application.port.out.GetRoleListPort;
import CGBVP.emergency.intranet.session_manager.domain.Role;
import lombok.RequiredArgsConstructor;

import java.util.List;

@UseCase
@RequiredArgsConstructor
public class GetRoleListService implements GetRoleListUseCase {
    private final GetRoleListPort getRoleListPort;
    @Override
    public List<Role> execute() {
        return getRoleListPort.getList();
    }
}
