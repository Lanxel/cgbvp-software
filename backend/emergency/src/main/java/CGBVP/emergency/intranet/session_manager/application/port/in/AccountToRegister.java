package CGBVP.emergency.intranet.session_manager.application.port.in;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AccountToRegister {
    private Long userId;
    private String username;
    private String password;
    private String domain;
    private String type;
    private String imagePath;
}
