package CGBVP.emergency.intranet.emergency.application.port.in;

public interface CloseEmergencyUseCase {
    void execute(Long emergencyId);
}
