package CGBVP.emergency.intranet.emergency.application.port;

import CGBVP.emergency.commons.hexagonal.UseCase;
import CGBVP.emergency.intranet.emergency.application.port.in.GetEmergencyTypeListUseCase;
import CGBVP.emergency.intranet.emergency.application.port.out.GetEmergencyTypeListPort;
import CGBVP.emergency.intranet.emergency.domain.EmergencyType;
import lombok.RequiredArgsConstructor;

import java.util.List;

@UseCase
@RequiredArgsConstructor
public class GetEmergencyTypeListService implements GetEmergencyTypeListUseCase {
    private final GetEmergencyTypeListPort getEmergencyTypeListPort;
    @Override
    public List<EmergencyType> execute() {
        return getEmergencyTypeListPort.getList();
    }
}
