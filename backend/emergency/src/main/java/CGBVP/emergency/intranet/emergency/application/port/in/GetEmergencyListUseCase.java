package CGBVP.emergency.intranet.emergency.application.port.in;

import CGBVP.emergency.intranet.emergency.domain.Emergency;

import java.util.List;

public interface GetEmergencyListUseCase {
    List<Emergency> execute(String state);
}
