package CGBVP.emergency.intranet.logistic.application.port.in;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UnitToRegister {
    private Long firefighterCompanyId;
    private String plate;
    private String code;
    private String type;
    private String description;
    private String imagePath;
}
