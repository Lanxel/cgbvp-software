package CGBVP.emergency.intranet.transcriber.application.port.in;


import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;

public interface TranscribeMessageUseCase {
    String execute(MultipartFile inputAudio) throws FileNotFoundException, InterruptedException;
}
