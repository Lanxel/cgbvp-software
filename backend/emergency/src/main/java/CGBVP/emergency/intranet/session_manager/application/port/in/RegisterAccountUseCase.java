package CGBVP.emergency.intranet.session_manager.application.port.in;

import CGBVP.emergency.intranet.session_manager.domain.Account;

public interface RegisterAccountUseCase {
    Account execute(AccountToRegister accountToRegister);
}
