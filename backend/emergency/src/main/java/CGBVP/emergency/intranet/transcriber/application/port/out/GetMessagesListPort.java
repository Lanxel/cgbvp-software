package CGBVP.emergency.intranet.transcriber.application.port.out;

import CGBVP.emergency.intranet.transcriber.domain.Message;

import java.util.List;

public interface GetMessagesListPort {
    List<Message> getList(Long emergencyId);
}
