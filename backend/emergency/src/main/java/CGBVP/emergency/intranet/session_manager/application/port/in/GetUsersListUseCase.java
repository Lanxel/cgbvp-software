package CGBVP.emergency.intranet.session_manager.application.port.in;

import CGBVP.emergency.intranet.session_manager.domain.User;

import java.util.List;

public interface GetUsersListUseCase {
    List<User> execute(String name);
    List<User> execute();
}
