package CGBVP.emergency.intranet.emergency.domain;


import CGBVP.emergency.intranet.session_manager.domain.User;
import CGBVP.emergency.intranet.transcriber.domain.Message;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class Comment {
    private Long id;
    private Message message;
    private User user;
    private String text;
    private Timestamp createdAt;
    private Timestamp updatedAt;
}
