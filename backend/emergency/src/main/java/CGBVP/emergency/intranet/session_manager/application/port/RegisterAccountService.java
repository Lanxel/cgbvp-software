package CGBVP.emergency.intranet.session_manager.application.port;

import CGBVP.emergency.commons.hexagonal.UseCase;
import CGBVP.emergency.intranet.session_manager.application.port.in.AccountToRegister;
import CGBVP.emergency.intranet.session_manager.application.port.in.RegisterAccountUseCase;
import CGBVP.emergency.intranet.session_manager.application.port.out.RegisterAccountPort;
import CGBVP.emergency.intranet.session_manager.domain.Account;
import lombok.RequiredArgsConstructor;

@UseCase
@RequiredArgsConstructor
public class RegisterAccountService implements RegisterAccountUseCase {
    private final RegisterAccountPort registerAccountPort;
    @Override
    public Account execute(AccountToRegister accountToRegister) {
        return registerAccountPort.register(accountToRegister);
    }
}
