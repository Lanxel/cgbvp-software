package CGBVP.emergency.intranet.emergency.domain;

import lombok.Data;

import java.util.List;

@Data
public class EmergencyType {
    private Long id;
    private String name;
    private String code;
    private List<EmergencySubType> subTypeList;
}
