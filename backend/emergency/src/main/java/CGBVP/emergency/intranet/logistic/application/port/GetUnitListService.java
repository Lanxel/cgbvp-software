package CGBVP.emergency.intranet.logistic.application.port;

import CGBVP.emergency.commons.hexagonal.UseCase;
import CGBVP.emergency.intranet.logistic.application.port.in.GetUnitListUseCase;
import CGBVP.emergency.intranet.logistic.application.port.out.GetUnitListPort;
import CGBVP.emergency.intranet.logistic.domain.Unit;
import lombok.RequiredArgsConstructor;

import java.util.List;

@UseCase
@RequiredArgsConstructor
public class GetUnitListService implements GetUnitListUseCase {
    private final GetUnitListPort getUnitListPort;

    @Override
    public List<Unit> execute(String name) {
        return getUnitListPort.getUnits(name);
    }

    @Override
    public List<Unit> execute() {
        return getUnitListPort.getUnits();
    }
}
