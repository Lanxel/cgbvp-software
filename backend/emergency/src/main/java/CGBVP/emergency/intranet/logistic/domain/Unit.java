package CGBVP.emergency.intranet.logistic.domain;

import CGBVP.emergency.intranet.emergency.domain.FireCompany;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class Unit {
    private long id;
    private FireCompany firefighterCompany;
    private String plate;
    private String code;
    private String type;
    private String description;
    private UnitState state;
    private String imagePath;
    private Timestamp createdAt;
    private Timestamp updatedAt;
}
