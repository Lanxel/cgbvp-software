package CGBVP.emergency.intranet.transcriber.application.port.in;

import CGBVP.emergency.intranet.transcriber.domain.DefaultText;

import java.util.List;

public interface GetDefaultTextListUseCase {
    List<DefaultText> execute(String text);
    List<DefaultText> execute();
}
