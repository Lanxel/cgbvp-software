package CGBVP.emergency.intranet.session_manager.application.port;

import CGBVP.emergency.commons.hexagonal.UseCase;
import CGBVP.emergency.intranet.session_manager.application.port.in.GetUserUseCase;
import CGBVP.emergency.intranet.session_manager.application.port.out.GetUserPort;
import CGBVP.emergency.intranet.session_manager.domain.User;
import lombok.RequiredArgsConstructor;


@UseCase
@RequiredArgsConstructor
public class GetUserService implements GetUserUseCase {
    private final GetUserPort getUserPort;
    @Override
    public User execute(Long userId) {
        return getUserPort.getUser(userId);
    }
}
