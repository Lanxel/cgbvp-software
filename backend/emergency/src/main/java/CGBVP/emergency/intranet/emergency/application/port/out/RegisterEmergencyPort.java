package CGBVP.emergency.intranet.emergency.application.port.out;

import CGBVP.emergency.intranet.emergency.application.port.in.EmergencyToRegister;
import CGBVP.emergency.intranet.emergency.domain.Emergency;

public interface RegisterEmergencyPort {
    Emergency register(EmergencyToRegister emergencyToRegister);
}
