package CGBVP.emergency.intranet.emergency.application.port.out;

public interface CloseEmergencyPort {
    void closeEmergency(Long emergencyId);
}
