package CGBVP.emergency.intranet.transcriber.application.port.in;

import CGBVP.emergency.intranet.transcriber.domain.Message;

import java.util.List;

public interface GetMessagesListUseCase {
    List<Message> execute(Long emergencyId);
}
