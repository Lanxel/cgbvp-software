package CGBVP.emergency.intranet.logistic.application.port.out;

import CGBVP.emergency.intranet.logistic.domain.Unit;

import java.util.List;

public interface GetUnitListPort {
    List<Unit> getUnits(String name);
    List<Unit> getUnits();
}
