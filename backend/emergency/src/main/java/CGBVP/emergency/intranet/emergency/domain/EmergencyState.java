package CGBVP.emergency.intranet.emergency.domain;

public enum EmergencyState {
    ASIGNANDO,
    ATENDIENDO,
    CERRADO
}
