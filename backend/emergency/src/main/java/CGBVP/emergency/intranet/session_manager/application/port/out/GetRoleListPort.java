package CGBVP.emergency.intranet.session_manager.application.port.out;

import CGBVP.emergency.intranet.session_manager.domain.Role;

import java.util.List;

public interface GetRoleListPort {
    List<Role> getList();
}
