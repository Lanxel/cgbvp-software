package CGBVP.emergency.intranet.session_manager.application.port.out;

import CGBVP.emergency.intranet.session_manager.application.port.in.UserToRegister;
import CGBVP.emergency.intranet.session_manager.domain.User;

public interface RegisterUserPort {
    User register(UserToRegister userToRegister);
}
