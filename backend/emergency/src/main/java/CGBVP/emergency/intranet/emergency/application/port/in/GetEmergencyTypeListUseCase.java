package CGBVP.emergency.intranet.emergency.application.port.in;

import CGBVP.emergency.intranet.emergency.domain.EmergencyType;

import java.util.List;

public interface GetEmergencyTypeListUseCase {
    List<EmergencyType> execute();
}
