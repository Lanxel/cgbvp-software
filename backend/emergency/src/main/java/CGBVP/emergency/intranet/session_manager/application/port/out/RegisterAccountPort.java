package CGBVP.emergency.intranet.session_manager.application.port.out;

import CGBVP.emergency.intranet.session_manager.application.port.in.AccountToRegister;
import CGBVP.emergency.intranet.session_manager.domain.Account;

public interface RegisterAccountPort{
    Account register(AccountToRegister accountToRegister);
}
