import React, {useState} from "react";

import { makeStyles } from "@material-ui/core/styles";
import styles from "../../styles/jss/nextjs-material-kit/homeStyle";
import PageLayout from "../../components/PageLayout/PageLayout";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Button from "../../components/CustomButtons/Button";
import Link from "next/link";
import Primary from "../../components/Typography/Primary";
import {SearchOutlined} from "@material-ui/icons";
import Paper from "@material-ui/core/Paper";
import {primaryColor} from "../../styles/jss/nextjs-material-kit";
import {Container, TextField} from "@material-ui/core";
import {getUnitsList, getUnitsListByCode} from "../../hooks/api/units";

const useStyles = makeStyles(styles);

export default function Index() {
  const classes = useStyles();
  const [searchString, setSearchString] = useState("");
  const {unitsList, isLoading, error} = searchString!==""? getUnitsListByCode(searchString): getUnitsList();

  const columns = [
    { id: 'code', label: 'Código', minWidth: 100},
    { id: 'type', label: 'Tipo', minWidth: 100},
    {
      id: 'description',
      label: 'Descripción',
      minWidth: 200,
    },
    {
      id: 'state',
      label: 'Estado',
      minWidth: 150,
    },
    {
      id: 'firefighterCompany',
      label: 'Compañía de bomberos',
      minWidth: 170,
    },
  ];

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const dataGrid = (
      <Paper className={classes.root}>
        <TableContainer className={classes.container}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow >
                {columns.map((column) => (
                    <TableCell
                        key={column.id}
                        align={column.align}
                        style={{ minWidth: column.minWidth, backgroundColor: primaryColor, color:"white" }}
                    >
                      {column.label}
                    </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {!isLoading && unitsList.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => {
                return (
                    <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                      {columns.map((column) => {
                        const value = row[column.id] instanceof Object? row[column.id]["name"]: row[column.id];
                        return (
                            <TableCell key={column.id} align={column.align}>
                              {column.format && typeof value === 'number' ? column.format(value) : value}
                            </TableCell>
                        );
                      })}
                    </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
        {!isLoading && <TablePagination
            rowsPerPageOptions={[10, 25, 100]}
            component="div"
            count={unitsList.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
        />}
      </Paper>
  );
  return (
      <div>
        <PageLayout title={"Home"}>
          <Container>
            <div className={classes.topComponents}>
              <Primary>
                <h4  style={{lineHeight: "1em", fontWeight:"600"}}>Administrar unidades</h4>
              </Primary>
              <Link href="/new-unit">  
                <Button style={{maxHeight: "30px", maxWidth: "150px", marginLeft:"10px"}}>+ Agregar Unidades</Button>
              </Link>
            </div>
            <div className={classes.topTabsContainer}>
              <SearchOutlined style={{alignSelf:"center"}}/>
              <TextField id="search-input" label="Buscar por código" variant="filled" color={"secondary"} onChange={(event) => setSearchString(event.target.value)}/>
            </div>
            <div className={classes.tableContainer}>
              {dataGrid}
            </div>
          </Container>
        </PageLayout>
      </div>
  );
}