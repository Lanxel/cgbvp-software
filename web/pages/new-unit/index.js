import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import styles from "../../styles/jss/nextjs-material-kit/homeStyle";
import PageLayout from "../../components/PageLayout/PageLayout";
import {Container} from "@material-ui/core";
import Button from "../../components/CustomButtons/Button";
import Card from "../../components/Card/Card";
import Link from "next/link";
import NewUnitFormCardBody from "../../components/Card/NewUnitFormCardBody";

const useStyles = makeStyles(styles);


export default function editUser() {
  const classes = useStyles();

  return (
      <div>
        <PageLayout title={"Crear Unidad"}>
          <Container>
            <div className={classes.topComponents}>
              <div></div>
              <Link href={"/units"}>
                <Button color="primary" style={{maxHeight: "30px", marginLeft:"10px"}}>Volver</Button>
              </Link>

            </div>
            <div className={classes.cardFlexContainer}>
              <Card style={{maxWidth: "700px"}}>
                <NewUnitFormCardBody></NewUnitFormCardBody>
              </Card>
            </div>
          </Container>
        </PageLayout>
      </div>
  );
}