import React from "react";

import {makeStyles} from "@material-ui/core/styles";
import styles from "../../../styles/jss/nextjs-material-kit/homeStyle";
import PageLayout from "../../../components/PageLayout/PageLayout";
import Button from "../../../components/CustomButtons/Button";
import Link from "next/link";
import Primary from "../../../components/Typography/Primary";
import {Container} from "@material-ui/core";
import TimeLine from "../../../components/TimeLine/TimeLine";
import {getMessagesList} from "../../../hooks/api/messages";
import {useRouter} from "next/router";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Slide from "@material-ui/core/Slide";
import Divider from "@material-ui/core/Divider";
import {closeEmergency} from "../../../hooks/api/emergency";


const useStyles = makeStyles(styles);

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
});


export default function emergencyPage() {
  const classes = useStyles();
  const router = useRouter();
  const {id: emergencyId, state: emergencyState} = router.query;
  if (!emergencyId) {
    return (
        <div>ERROR</div>
    );
  }

  const {messagesList, error, isLoading} = getMessagesList(emergencyId);

  const [modal, setModal] = React.useState(false);

  const handleRequest = () => {
    return closeEmergency(emergencyId);
  };

  const modalComponent = (
      <Dialog
          open={modal}
          TransitionComponent={Transition}
          keepMounted
          onClose={() => setModal(false)}
          aria-labelledby="modal-slide-title"
          aria-describedby="modal-slide-description"
          style={{minWidth:"300"}}
      >
        <DialogContent
            id="modal-slide-description"
            style={{padding: "1.5rem 2rem", fontWeight: "600", textAlign:"center"}}
        >
           Al realizar esta acción la Emergencia cambiara a estado CERRADO ¿Seguro que quiere concluir la emergencia?
        </DialogContent>
        <DialogActions
            style={{justifyContent:"center"}}
        >
          <Button onClick={() => setModal(false)}>Cancelar</Button>
          <Link href={"/home"}>
            <Button color="primary" onClick={handleRequest}>
              Aceptar
            </Button>
          </Link>
        </DialogActions>
      </Dialog>
  );

  return (
      <div>
        <PageLayout title={"Emergencias"}>
          <Container>
            <div className={classes.topComponents}>
              <Primary>
                <h4  style={{lineHeight: "1em", fontWeight:"600"}}>LISTA DE MENSAJES</h4>
              </Primary>
              { emergencyState==="ATENDIENDO" &&  <Button color="primary" style={{maxHeight: "30px", maxWidth: "150px", marginLeft:"10px"}} onClick={() => setModal(true)}>Cerrar Emergencia</Button>}
              { emergencyState==="ATENDIENDO" &&
                <Link href={"/emergency/" + emergencyId + "/create-message"}>
                  <Button style={{maxHeight: "30px", maxWidth: "150px", marginLeft:"10px"}}>+ Agregar mensaje</Button>
                </Link>
                }
            </div>
            <div style={{justifyContent: "initial"}}>
              { emergencyState==="CERRADO" &&
                  <div style={{textAlign: "center"}}>
                    <Primary><h2>La Emergencia ha concluido</h2></Primary>
                    <Divider style={{marginTop: "20px", height: "3px"}}/>
                  </div>
              }
              <TimeLine data={messagesList} loading={isLoading}></TimeLine>
            </div>
          </Container>
        </PageLayout>
        {modalComponent}
      </div>
  );
}