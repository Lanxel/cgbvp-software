import React from "react";

import { makeStyles } from "@material-ui/core/styles";
import styles from "../../../styles/jss/nextjs-material-kit/homeStyle";
import PageLayout from "../../../components/PageLayout/PageLayout";
import {Container} from "@material-ui/core";
import Button from "../../../components/CustomButtons/Button";
import Card from "../../../components/Card/Card";
import NewMessageCardBody from "../../../components/Card/NewMessageCardBody";
import {useRouter} from "next/router";

const useStyles = makeStyles(styles);


export default function CreateMessage() {
  const classes = useStyles();
  const router = useRouter();
  const {id: emergencyId} = router.query;

  const handleClick = (event) => {
    event.preventDefault();
    router.push({pathname:"/emergency/" + emergencyId, query: {state: "ATENDIENDO"}});
  };

  return (
      <div>
        <PageLayout title={"Home"}>
          <Container>
            <div className={classes.topComponents}>
              <Button onClick={handleClick} style={{maxHeight: "30px", maxWidth: "150px", marginLeft:"10px"}}>Volver</Button>
            </div>
            <div className={classes.cardsContainer}>
              <Card>
                <NewMessageCardBody></NewMessageCardBody>
              </Card>
            </div>
          </Container>
        </PageLayout>
      </div>
  );
}