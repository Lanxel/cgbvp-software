import React, {useState} from "react";

import { makeStyles } from "@material-ui/core/styles";
import styles from "../../styles/jss/nextjs-material-kit/homeStyle";
import PageLayout from "../../components/PageLayout/PageLayout";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Button from "../../components/CustomButtons/Button";
import Link from "next/link";
import Primary from "../../components/Typography/Primary";
import {SearchOutlined} from "@material-ui/icons";
import Paper from "@material-ui/core/Paper";
import {primaryColor} from "../../styles/jss/nextjs-material-kit";
import {Container, TextField} from "@material-ui/core";
import {getUsersList, getUsersListByName} from "../../hooks/api/user";



const useStyles = makeStyles(styles);


export default function Index() {
  const classes = useStyles();
  const [searchString, setSearchString] = useState("");
  const {usersList, isLoading, error} = searchString!=""? getUsersListByName(searchString): getUsersList();

  const columns = [
    { id: 'name', label: 'Nombre', minWidth: 170},
    { id: 'role', label: 'Rol', minWidth: 100},
    {
      id: 'position',
      label: 'Cargo',
      minWidth: 170,
    },
    {
      id: 'contactNumber',
      label: 'Celular',
      minWidth: 170,
    },
    {
      id: 'bloodType',
      label: 'Tipo de sangre',
      minWidth: 170,
    },
    {
      id: 'dni',
      label: 'DNI',
      minWidth: 170,
      sortable: true,
    },
  ];

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const dataGrid = (
      <Paper className={classes.root}>
        <TableContainer className={classes.container}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow >
                {columns.map((column) => (
                    <TableCell
                        key={column.id}
                        align={column.align}
                        style={{ minWidth: column.minWidth, backgroundColor: primaryColor, color:"white" }}
                    >
                      {column.label}
                    </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {!isLoading && usersList.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => {
                return (
                    <TableRow hover role="checkbox" tabIndex={-1} key={row.dni}>
                      {columns.map((column) => {
                        const value = row[column.id];
                        return (
                            <Link href={"/users/"+ row.id} key={column.id}>
                              <TableCell key={column.id} align={column.align}>
                                {column.format && typeof value === 'number' ? column.format(value) : value}
                              </TableCell>
                            </Link>
                        );
                      })}
                    </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
        {!isLoading && <TablePagination
            rowsPerPageOptions={[10, 25, 100]}
            component="div"
            count={usersList.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
        />}
      </Paper>
  );
  return (
      <div>
        <PageLayout title={"Home"}>
          <Container>
            <div className={classes.topComponents}>
              <Primary>
                <h4  style={{lineHeight: "1em", fontWeight:"600"}}>Administrar usuarios</h4>
              </Primary>
              <Link href="/new-user">
                <Button style={{maxHeight: "30px", maxWidth: "150px", marginLeft:"10px"}}>+ Agregar usuarios</Button>
              </Link>
            </div>
            <div className={classes.topTabsContainer}>
              <SearchOutlined style={{alignSelf:"center"}}/>
              <TextField id="search-input" label="Buscar nombre" variant="filled" color={"secondary"} onChange={(event) => setSearchString(event.target.value)}/>
            </div>
            <div className={classes.tableContainer}>
              {dataGrid}
            </div>
          </Container>
        </PageLayout>
      </div>
  );
}