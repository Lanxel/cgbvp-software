import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import styles from "../../styles/jss/nextjs-material-kit/homeStyle";
import PageLayout from "../../components/PageLayout/PageLayout";
import {Container} from "@material-ui/core";
import Button from "../../components/CustomButtons/Button";
import Card from "../../components/Card/Card";
import NewUserFormCardBody from "../../components/Card/NewUserFormCardBody";
import Link from "next/link";

const useStyles = makeStyles(styles);


export default function editUser() {
  const classes = useStyles();

  const link = `/users`;
  return (
      <div>
        <PageLayout title={"Crear usuario"}>
          <Container>
            <div className={classes.topComponents}>
              <div></div>
              <Link href={link}>
                <Button color="primary" style={{maxHeight: "30px", marginLeft:"10px"}}>Volver</Button>
              </Link>

            </div>
            <div className={classes.cardFlexContainer}>
              <Card style={{maxWidth: "700px"}}>
                <NewUserFormCardBody></NewUserFormCardBody>
              </Card>
            </div>
          </Container>
        </PageLayout>
      </div>
  );
}