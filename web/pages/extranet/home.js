import React from "react";

import { makeStyles } from "@material-ui/core/styles";
import styles from "../../styles/jss/nextjs-material-kit/homeStyle";
import PageLayoutExtranet from "../../components/PageLayout/PageLayoutExtranet";
import {Container} from "@material-ui/core";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import EmergencyCardBodyExtranet from "../../components/Card/EmergencyCardBodyExtranet";
import Card from "../../components/Card/Card";
import Primary from "../../components/Typography/Primary";
import {getEmergencyList} from "../../hooks/api/emergency";

const useStyles = makeStyles(styles);


export default function ExtranetHome() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const state = value? "CERRADO": "ATENDIENDO";
  const {emergencyList, isLoading, error} = getEmergencyList(state);
  return (
      <div>
        <PageLayoutExtranet title={"Home"}>
          <Container>
            <div className={classes.topComponents}>
              <Primary>
                <h4  style={{lineHeight: "1em", fontWeight:"600"}}>LISTA DE EMERGENCIAS</h4>
              </Primary>
            </div>
            <div className={classes.topTabsContainer}>
              <Tabs
                  value={value}
                  indicatorColor="secondary"
                  onChange={handleChange}
              >
                <Tab label="En progreso" />
                <Tab label="Finalizados" />
              </Tabs>
            </div>
            <div className={classes.emergencyCardsContainer}>
              <div className={classes.emergencyCardStyle}>
                { !isLoading && emergencyList.map((value) =>
                    <Card style={{maxWidth:"300px"}} key={value.id}>
                      <EmergencyCardBodyExtranet data={value}></EmergencyCardBodyExtranet>
                    </Card>
                )
                }
              </div>
            </div>
          </Container>
        </PageLayoutExtranet>
      </div>
  );
}
