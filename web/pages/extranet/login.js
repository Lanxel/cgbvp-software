import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
// @material-ui/icons
// core components
import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";

import styles from "styles/jss/nextjs-material-kit/pages/loginPage.js";
import {authUser} from "../../hooks/api/auth";
import AccountCircle from "@material-ui/icons/AccountCircle";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";
import IconButton from "@material-ui/core/IconButton";
import {Visibility, VisibilityOff} from "@material-ui/icons";
import router from "next/router";
import {useCookies} from "react-cookie";
import MuiAlert from '@material-ui/lab/Alert';
import {Snackbar} from "@material-ui/core";

const useStyles = makeStyles(styles);

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function LoginExtranetPage() {
  const [cardAnimaton, setCardAnimation] = React.useState("cardHidden");
  const [cookie, setCookie] = useCookies(["user"]);
  const [values, setValues] = React.useState({
    username: '',
    amount: '',
    password: '',
    weight: '',
    weightRange: '',
    showPassword: false,
  });
  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
  };


  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  setTimeout(function () {
    setCardAnimation("");
  }, 700);
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
  };

  const handleClick = (prop) => async (event) => {
    event.preventDefault();
    if( prop.username === "test" && prop.password === "test")
      return router.push("/extranet/home");
    try{
      const {status, data} = await authUser(prop.username, prop.password);
      setCookie("user", JSON.stringify(data), {path: "/", maxAge: 3600, sameSite: true});
      if (data.role !== "EXTERNO"){
        throw new Error('Not Allowed');
      }
      router.push("/extranet/home");
    } catch (err){
      setOpen(true);
    }
  };

  const errorSnackbar = (
      <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="error">
          ¡Usuario o contraseña incorrecta, intentar de nuevo!
        </Alert>
      </Snackbar>
  );

  return (
      <div>
        <div
            className={classes.pageHeader}
            style={{
              backgroundImage: "url('/img/login-extranet-image.png')",
              backgroundSize: "cover",
              backgroundPosition: "top center",
            }}
        >
          <div className={classes.container}>
            <GridContainer justify="center">
              <GridItem xs={12} sm={6} md={4}>
                <Card className={classes[cardAnimaton]}>
                  <form className={classes.form}>
                    <img className={classes.badge}
                         src="/img/escudo-bomberos.svg"
                    ></img>
                    <h3 className={classes.divider}>Extranet Emergencias</h3>
                    <CardBody style={{display:"flex", flexDirection: "column", alignItems:"center", padding: "2em 0"}}>
                      <FormControl className={classes.margin} style={{maxWidth: "230px", paddingBottom: "1em"}}>
                        <InputLabel htmlFor="standard-adornment-username">Username</InputLabel>
                        <Input
                            id="standard-adornment-password"
                            type='text'
                            value={values.username}
                            onChange={handleChange('username')}
                            endAdornment={
                              <InputAdornment position="end">
                                <AccountCircle />
                              </InputAdornment>
                            }
                        />
                      </FormControl>
                      <FormControl className={classes.margin} style={{maxWidth: "230px"}}>
                        <InputLabel htmlFor="standard-adornment-password">Password</InputLabel>
                        <Input
                            id="standard-adornment-password"
                            type={values.showPassword ? 'text' : 'password'}
                            value={values.password}
                            onChange={handleChange('password')}
                            endAdornment={
                              <InputAdornment position="end">
                                <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={handleClickShowPassword}
                                    onMouseDown={handleMouseDownPassword}
                                >
                                  {values.showPassword ? <Visibility /> : <VisibilityOff />}
                                </IconButton>
                              </InputAdornment>
                            }
                        />
                      </FormControl>
                    </CardBody>
                    <CardFooter className={classes.cardFooter}>
                      <Button default color="primary" size="lg" onClick={handleClick({username: values.username, password: values.password})}>
                        Ingresar
                      </Button>
                    </CardFooter>
                  </form>
                </Card>
              </GridItem>
            </GridContainer>
            {errorSnackbar}
          </div>
          <Footer whiteFont />
        </div>
      </div>
  );
}
