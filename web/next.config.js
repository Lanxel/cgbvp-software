const withPlugins = require("next-compose-plugins");
const withImages = require("next-images");
const webpack = require("webpack");
const path = require("path");

// eslint-disable-next-line no-undef
module.exports = withPlugins([[withImages]], {
  webpack(config, options) {
    config.resolve.modules.push(path.resolve("./"));
    return config;
  },
  publicRuntimeConfig: {
    apiUrl: process.env.BACKEND_API_URL
  }
});
