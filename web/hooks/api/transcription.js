import {useConfig} from './config';
import axios from 'axios';

export async function useTranscriber(file) {
  const {apiUrl} = useConfig();
  //const {mutate} = useSWRConfig();

  let formData = new FormData();
  formData.append("file", file);

  return axios.post(`${apiUrl}/messages/transcribe`, formData, {headers: {'Content-Type': 'multipart/form-data'}});
}
