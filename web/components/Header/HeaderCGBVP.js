import React from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import {defaultFont} from "../../styles/jss/nextjs-material-kit";
import {Hidden} from "@material-ui/core";
import Link from "next/link";
import PropTypes from "prop-types";
import PageLayout from "../PageLayout/PageLayout";
import {useCookies} from "react-cookie";
import {getUnitsList} from "../../hooks/api/units";
import {getCookie} from "cookies-next";
import Button from "../CustomButtons/Button";

const drawerWidth = 300;

const useStyles = makeStyles((styles) => ({
  root: {
    display: 'flex',
    height: "100vh",
  },
  appBar: {
    [styles.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
    },
    background: '#C00812',
  },
  toolbar: styles.mixins.toolbar,
  menuButton: {
    marginRight: styles.spacing(2),
    [styles.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
    background: '#404040',
    height: "100vh",
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: styles.spacing(0, 1),
    // necessary for content to be below app bar
    ...styles.mixins.toolbar,
    justifyContent: 'flex-end',
    background: "#C00812",
  },
  content: {
    flexGrow: 1,
    height: "100vh",
    padding: styles.spacing(3),
  },
  contentList: {
    ...defaultFont,
    lineHeight: "20px",
    fontSize: "18px",
    borderRadius: "3px",
    textTransform: "none",
    textAlign: "initial",
    color: "white",
    padding: "8px 10px",
    letterSpacing: "unset",
  },
  listItem: {
    "&:hover,&:focus": {
      color: "#929698",
      background: "transparent",
    },
    justifyContent: "space-between"
  },
  badge: {
    marginLeft: "auto",
    marginRight: "auto",
    width: "35px",
    height: "45px",
  },
  divider: {
    background: "#929698",
  },
  profile:{
    margin: "30px auto 30px auto",
    color: "white",
    textAlign: "center",
  },
  profileImage:{
    width: "120px",
    height: "120px",
    borderRadius: "100%",
  },
  h4:{
    fontWeight: 700,
    fontSize: "18px",
    margin: "15px 0px 5px 0"
  }
}));

// eslint-disable-next-line react/prop-types
export default function HeaderCGBVP(props) {
  const classes = useStyles();
  const { children } = props;
  const cookie = getCookie("user");
  const user = JSON.parse(cookie??"{}");
  const theme = useTheme();
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const drawerButtons = [
    {id: 1, name: "Inicio", link:"/home" , color: "transparent"},
    {id: 2, name: "Administrar unidades", link:"/units",  color: "transparent"},
    {id: 3, name: "Administrar usuarios", link:"/users",  color: "transparent"},
    //{id: 4, name: "Cerrar Sesión", link:"/login" , color: "#transparent"}
  ];
  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const drawer = (
    <div>
      <div className={classes.drawerHeader}>
        <IconButton onClick={handleDrawerToggle}>
          {theme.direction === 'ltr' ? <ChevronLeftIcon htmlColor="white"/> : <ChevronRightIcon htmlColor="white"/>}
        </IconButton>
      </div>
      <div className={classes.profile}>
        <img className={classes.profileImage}
             src="/img/profile-image.png"
             alt={""}></img>
        <div className={classes.h4}> {user?.name??""} </div>
        <div>Cargo de {user?.position??"" } </div>
      </div>
      <Divider className={classes.divider}/>
      <List className={classes.contentList}>
        {drawerButtons.map((button) => (
            <Link href={button.link} key={button.id}>
              <ListItem className={classes.listItem} style={{background: button.color}} button >
                <ListItemText primary={button.name} />
                <ListItemIcon >{<ChevronRightIcon htmlColor={"white"} />}</ListItemIcon>
              <Divider className={classes.divider} absolute/>
              </ListItem>
            </Link>
        ))}
      </List>
      <div style={{display: "flex", justifyContent: "center", marginTop: "5rem"}}>
        <Link href={"/login"}>
          <Button color={"primary"}> Cerrar Sesión</Button>
        </Link>
      </div>

    </div>
  );

  return (
      <div className={classes.root}>
        <CssBaseline />
        <AppBar
            position="fixed"
            className={clsx(classes.appBar)}
        >
          <Toolbar>
            <IconButton
                color="inherit"
                aria-label="open drawer"
                onClick={handleDrawerToggle}
                className={clsx(classes.menuButton)}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" noWrap>
              CGBVP
            </Typography>
            <img className={classes.badge}
                 src="/img/escudo-bomberos.svg"
                 alt={""}></img>
          </Toolbar>
        </AppBar>
        <Hidden mdUp>
          <Drawer
              className={classes.drawer}
              variant="temporary"
              anchor="left"
              open={mobileOpen}
              classes={{
                paper: classes.drawerPaper,
              }}
              ModalProps={{
                keepMounted: true, // Better open performance on mobile.
              }}
          >{drawer}
          </Drawer>
        </Hidden>
        <Hidden smDown implementation="css">
          <Drawer
              className={classes.drawer}
              variant="permanent"
              anchor="left"
              open={mobileOpen}
              classes={{
                paper: classes.drawerPaper,
              }}
          >{drawer}
          </Drawer>
        </Hidden>
        <main className={classes.content}>
          <div className={classes.toolbar}/>
            {children}
        </main>
      </div>
  );
}

HeaderCGBVP.propTypes = {
  children: PropTypes.node,
  data: PropTypes.object,
};