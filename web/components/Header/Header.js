import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Drawer from "@material-ui/core/Drawer";
import { isMobile } from 'react-device-detect';
// @material-ui/icons
import Menu from "@material-ui/icons/Menu";
// core components
import styles from "styles/jss/nextjs-material-kit/components/headerStyle.js";
import {Hidden} from "@material-ui/core";

const useStyles = makeStyles(styles);

export default function Header(props) {
  const classes = useStyles();
  const [drawerOpen, setDrawerOpen] = React.useState(!isMobile);
  const handleDrawerToggle = () => {
    setDrawerOpen(!drawerOpen);
  };
  const { color, fixed, absolute } = props;
  const appBarClasses = classNames({
    [classes.appBar]: true,
    [classes[color]]: color,
    [classes.absolute]: absolute,
    [classes.fixed]: fixed,
  });
  return (
    <AppBar className={appBarClasses}>
      <Toolbar className={classes.container}>
        <IconButton className={classes.headerIcon}
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerToggle}
        >
          <Menu />
        </IconButton>
        <img className={classes.badge}
           src="/img/escudo-bomberos.svg"
        alt={""}></img>
      </Toolbar>
        <Drawer
          variant="persistent"
          anchor={"left"}
          open={drawerOpen}
          classes={{
            paper: classes.drawerPaper,
          }}
          onClose={handleDrawerToggle}
        >
          <div className={classes.appResponsive}>
          </div>
        </Drawer>
    </AppBar>
  );
}

Header.defaultProp = {
  color: "white",
};

Header.propTypes = {
  color: PropTypes.oneOf([
    "primary",
    "info",
    "success",
    "warning",
    "danger",
    "transparent",
    "white",
    "rose",
    "dark",
  ]),
  rightLinks: PropTypes.node,
  leftLinks: PropTypes.node,
  brand: PropTypes.string,
  fixed: PropTypes.bool,
  absolute: PropTypes.bool,
  // this will cause the sidebar to change the color from
  // props.color (see above) to changeColorOnScroll.color
  // when the window.pageYOffset is heigher or equal to
  // changeColorOnScroll.height and then when it is smaller than
  // changeColorOnScroll.height change it back to
  // props.color (see above)
  changeColorOnScroll: PropTypes.shape({
    height: PropTypes.number.isRequired,
    color: PropTypes.oneOf([
      "primary",
      "info",
      "success",
      "warning",
      "danger",
      "transparent",
      "white",
      "rose",
      "dark",
    ]).isRequired,
  }),
};
