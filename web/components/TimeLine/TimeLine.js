import React from "react";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// nodejs library that concatenates classes
import classNames from "classnames";
// material-ui core components
import Timeline from '@material-ui/lab/Timeline';
import TimelineItem from '@material-ui/lab/TimelineItem';
import TimelineSeparator from '@material-ui/lab/TimelineSeparator';
import TimelineConnector from '@material-ui/lab/TimelineConnector';
import TimelineContent from '@material-ui/lab/TimelineContent';
import TimelineOppositeContent from '@material-ui/lab/TimelineOppositeContent';
import TimelineDot from '@material-ui/lab/TimelineDot';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from "@material-ui/core/styles";

import styles from "styles/jss/nextjs-material-kit/components/footerStyle.js";
import MessageCardBody from "../Card/MessageCardBody";
import {useMediaQuery, useTheme} from "@material-ui/core";

const useStyles = makeStyles(styles);

export default function TimeLine(props) {
  const classes = useStyles();
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up('md'));
  const data = props.data;
  const isLoading = props.loading;

  return (
      <Timeline align={matches?'alternate':'left'} style={{width:"100%"}} >
        { !isLoading &&
          (data.map((value, index) =>
              <TimelineItem key={index}>
                <TimelineOppositeContent style={matches?{}:{display:"none"}}></TimelineOppositeContent>
                <TimelineSeparator >
                  <TimelineDot color="grey" style={matches?{height: "1.5rem", width: "1.5rem"}:{height: "1rem", width: "1rem"}}>
                  </TimelineDot>
                  <TimelineConnector />
                </TimelineSeparator>
                <TimelineContent>
                  <Paper elevation={3} className={classes.paper}>
                    <MessageCardBody data={value}></MessageCardBody>
                  </Paper>
                </TimelineContent>
              </TimelineItem>
          ))
        }
      </Timeline>
  );
}

TimeLine.propTypes = {
  whiteFont: PropTypes.bool,
  data: PropTypes.array,
  loading: PropTypes.bool,
};
