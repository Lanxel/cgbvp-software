import React, {useCallback, useState} from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// @material-ui/icons

// core components
import styles from "styles/jss/nextjs-material-kit/components/cardBodyStyle.js";
import CardBody from "./CardBody";
import {primaryColorVariant} from "../../styles/jss/nextjs-material-kit";
import Button from "../CustomButtons/Button";
import GridContainer from "../Grid/GridContainer";
import {TextField} from "@material-ui/core";
import Autocomplete from '@material-ui/lab/Autocomplete';
import {getEmergencyTypeList} from "../../hooks/api/emergencyType";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Link from "next/link";
import Slide from "@material-ui/core/Slide";
import {newMessagesList} from "../../hooks/api/messages";
import {getCookie} from "cookies-next";
import router from "next/router";
import {newEmergency} from "../../hooks/api/emergency";

const useStyles = makeStyles(styles);

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
});

export default function NewUserFormCardBody(props) {
  const classes = useStyles();
  const { className, children, ...rest } = props;
  const cardBodyClasses = classNames({
    [classes.userCardBody]: true,
    [className]: className !== undefined,
  });
  const [values, setValues] = React.useState({
    type: '',
    subType: '',
    description: '',
    district: '',
    address: '',
    unit: false,
  });
  const cookie = getCookie("user");
  const user = JSON.parse(cookie??"{}");
  const [subTypeList, setSubTypeList] = useState(null);
  const {types, isLoading, error} = getEmergencyTypeList();
  const handleAutocomplete = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.textContent });
  };

  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
  };

  const handleRequest = () => {
    const companyId = user.company.id;
    const res = newEmergency(companyId,values.type, values.subType, values.description, values.district, values.address);
    router.push("/home");
    return res;
  };

  const [modal, setModal] = React.useState(false);
  const modalComponent = (
      <Dialog
          open={modal}
          TransitionComponent={Transition}
          keepMounted
          onClose={() => setModal(false)}
          aria-labelledby="modal-slide-title"
          aria-describedby="modal-slide-description"
          style={{minWidth:"300"}}
      >
        <DialogContent
            id="modal-slide-description"
            style={{padding: "1.5rem 2rem", fontWeight: "600", textAlign:"center"}}
        >
          ¿Seguro que quiere registrar la emergencia?
        </DialogContent>
        <DialogActions
            style={{justifyContent:"center"}}
        >
          <Button onClick={() => setModal(false)}>Cancelar</Button>
          <Link href={"/home"}>
            <Button color="primary" onClick={handleRequest}>
              Aceptar
            </Button>
          </Link>
        </DialogActions>
      </Dialog>
  );
  const districtsList = [
    {name: "Ancón"},{name: "Ate"},{name: "Barranco"},{name: "Breña"},{name: "Carabayllo"},
    {name: "Cercado de Lima"},{name: "Chaclacayo"},{name: "Chorrillos"},{name: "Cieneguilla"},
    {name: "Comas"},{name: "El agustino"},{name: "Independencia"},{name: "Jesús maría"},
    {name: "La molina"},{name: "La victoria"},{name: "Lince"},{name: "Los olivos"},
    {name: "Lurigancho"},{name: "Lurín"},{name: "Magdalena del mar"},{name: "Miraflores"},
    {name: "Pachacámac"},{name: "Pucusana"},{name: "Pueblo libre"},{name: "Puente piedra"},
    {name: "Punta hermosa"},{name: "Punta negra"},{name: "Rímac"},{name: "San bartolo"},
    {name: "San borja"},{name: "San isidro"},{name: "San Juan de Lurigancho"},
    {name: "San Juan de Miraflores"},{name: "San Luis"},{name: "San Martin de Porres"},
    {name: "San Miguel"},{name: "Santa Anita"},{name: "Santa María del Mar"},
    {name: "Santa Rosa"},{name: "Santiago de Surco"},{name: "Surquillo"},
    {name: "Villa el Salvador"},{name: "Villa Maria del Triunfo"},
  ]
  return (
      <div className={cardBodyClasses} {...rest}>
        <CardBody className={classes.userFormBody}>
          <div className={classes.title} style={{color:primaryColorVariant, paddingTop: "15px"}}>Nueva Emergencia</div>
          <GridContainer style={{justifyContent: "center"}}>
            <div className={classes.labelClass}>
              {!isLoading && <Autocomplete
                  id="combo-box-demo"
                  options={types}
                  getOptionLabel={(option) => {setSubTypeList(option.subTypeList) ;return option.name;} }
                  style={{ width: "80%", marginLeft: "28px" }}
                  renderInput={(params) => <TextField {...params} label="Tipo" variant="standard" />}
                  closeIcon={false}
                  onChange={handleAutocomplete("type")}
              />}
            </div>
            <div className={classes.labelClass}>
              {!isLoading &&<Autocomplete
                  id="combo-box-demo"
                  options={subTypeList}
                  getOptionLabel={(option) => option.name}
                  style={{ width: "80%", marginLeft: "28px" }}
                  disabled={!subTypeList}
                  renderInput={(params) => <TextField {...params} label="Sub-Tipo" variant="standard" />}
                  closeIcon={false}
                  onChange={handleAutocomplete("subType")}
              />}
            </div>
            <div className={classes.labelClass}>
              <TextField required id="description-required" label="Descripción" color="secondary" onChange={handleChange("description")}  style={{width:"80%"}}/>
            </div>
            <div className={classes.labelClass}>
              <Autocomplete
                  id="combo-box-demo"
                  options={districtsList}
                  getOptionLabel={(option) => option.name }
                  style={{ width: "80%", marginLeft: "28px" }}
                  renderInput={(params) => <TextField {...params} label="Distrito" variant="standard" />}
                  closeIcon={false}
                  onChange={handleAutocomplete("district")}
              />
            </div>
            <div className={classes.labelClass}>
              <TextField required id="address-required" label="Dirección" color="secondary" onChange={handleChange("address")} style={{width:"80%"}}/>
            </div>
            {/*<div className={classes.labelClass}>
              <Autocomplete
                  id="combo-box-demo"
                  options={types}
                  getOptionLabel={(option) => option.title}
                  style={{ width: "80%", marginLeft: "28px" }}
                  renderInput={(params) => <TextField {...params} label="Unidad" variant="standard" />}
              />
            </div>*/}
          </GridContainer>
          <Button color="primary" onClick={() => setModal(true)} style={{margin: "1rem 2rem", width: "200px", alignSelf:"center"}}>Agregar Emergencia</Button>
          {children}
        </CardBody>
        {modalComponent}
      </div>
  );
}

NewUserFormCardBody.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
};
