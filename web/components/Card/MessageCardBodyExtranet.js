import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// @material-ui/icons

// core components
import styles from "styles/jss/nextjs-material-kit/components/cardBodyStyle.js";
import CardBody from "./CardBody";
import {primaryColorVariant} from "../../styles/jss/nextjs-material-kit";
import {ModeComment} from "@material-ui/icons";
import Button from "../CustomButtons/Button";

const useStyles = makeStyles(styles);

export default function MessageCardBodyExtranet(props) {
    const classes = useStyles();
    const { className, children, data, ...rest } = props;
    const cardBodyClasses = classNames({
        [classes.cardBodyMessage]: true,
        [className]: className !== undefined,
    });
    const date = new Date(data.createdAt).toLocaleString();

    const transcription = data.content.defaultText? data.content.defaultText.text: data.content.intranetText;

    return data.type !== "General"? (
        <div className={cardBodyClasses} {...rest}>
            <CardBody className={classes.messageBody}>
                <div className={classes.title} style={{color:primaryColorVariant}}>Compañia {data.company}</div>
                <div className={classes.contain} style={{padding: "0 0 5px 0", fontWeight:"500"}}>
                    {date} &nbsp;&nbsp;&nbsp; {data.user.name}
                </div>
                <div className={classes.description} >{transcription}</div>
                <Button className={classes.commentButton} type="button" color={"transparent"} size={"sm"} >
                    <ModeComment className={classes.icon}></ModeComment>
                    0 comentarios
                </Button>
            {children}
            </CardBody>
        </div>
    ): <div></div>;
}

MessageCardBodyExtranet.propTypes = {
    className: PropTypes.string,
    children: PropTypes.node,
    data: PropTypes.object,
};
