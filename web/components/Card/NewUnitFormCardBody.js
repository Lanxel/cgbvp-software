import React, {useCallback} from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// @material-ui/icons

// core components
import styles from "styles/jss/nextjs-material-kit/components/cardBodyStyle.js";
import CardBody from "./CardBody";
import {primaryColorVariant} from "../../styles/jss/nextjs-material-kit";
import Button from "../CustomButtons/Button";
import GridContainer from "../Grid/GridContainer";
import {TextField} from "@material-ui/core";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import FiberManualRecord from "@material-ui/icons/FiberManualRecord";

const useStyles = makeStyles(styles);

export default function NewUnitFormCardBody(props) {
  const classes = useStyles();
  const { className, children, ...rest } = props;
  const cardBodyClasses = classNames({
    [classes.userCardBody]: true,
    [className]: className !== undefined,
  });

  return (
      <div className={cardBodyClasses} {...rest}>
        <CardBody className={classes.userFormBody}>
          <div className={classes.title} style={{color:primaryColorVariant, paddingTop: "15px"}}>Nueva unidad</div>
          <GridContainer style={{justifyContent: "center"}}>
            <div className={classes.labelClass}>
              <TextField required id="company-required" label="Compañía" color="secondary" style={{width:"80%"}}/>
            </div>
            <div className={classes.labelClass}>
              <TextField required id="plate-required" label="Placa" color="secondary" style={{width:"80%"}}/>
            </div>
            <div className={classes.labelClass}>
              <TextField required id="code-required" label="Código" color="secondary" style={{width:"80%"}}/>
            </div>
            <div className={classes.labelClass}>
              <TextField required id="type-required" label="Tipo" color="secondary" style={{width:"80%"}}/>
            </div>
            <div className={classes.labelClass}>
              <TextField required id="description-required" label="Descripción" multiline rows={5} variant="outlined" color="secondary" style={{width:"80%", marginTop: "30px"}}/>
            </div>
          </GridContainer>
          <Button color="primary" style={{margin: "1rem 2rem", width: "200px", alignSelf:"center"}}>Crear Unidad</Button>
          {children}
        </CardBody>
      </div>
  );
}

NewUnitFormCardBody.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
};
