import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// @material-ui/icons

// core components
import styles from "styles/jss/nextjs-material-kit/components/cardBodyStyle.js";
import CardBody from "./CardBody";
import {primaryColor, primaryColorVariant} from "../../styles/jss/nextjs-material-kit";
import Button from "../CustomButtons/Button";
import router from "next/router";

const useStyles = makeStyles(styles);

export default function EmergencyCardBody(props) {
  const classes = useStyles();
  const { className, children, data, ...rest } = props;
  const cardBodyClasses = classNames({
    [classes.cardBodyEmergency]: true,
    [className]: className !== undefined,
  });
  const handleClick = (event) => {
    event.preventDefault();
    router.push({pathname:"/extranet/emergency/" + data.id, query: {state: data.emergencyState}});
  };

  const date = new Date(data.date).toLocaleString();
  return (
      <div className={cardBodyClasses} {...rest}>
        <CardBody className={classes.emergencyBody}>
          <div className={classes.title} style={{color:primaryColorVariant}}>Emergencia</div>
          <div className={classes.contain} style={{fontWeight:700}}>{data.type} / {data.subType} / {data.description}</div>
          <div className={classes.contain} style={{color:primaryColor, fontWeight:700}}>{data.emergencyState}</div>
          <div className={classes.contain}>{data.district}<br/></div>
          <div className={classes.contain}>{data.address}<br/></div>
          <div className={classes.contain}>{date}<br/></div>
          <div className={classes.contain}>{data.units}<br/></div>
          <Button color="primary" onClick={handleClick}>Ver información</Button>
          {children}
        </CardBody>
      </div>
  );
}

EmergencyCardBody.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
  data: PropTypes.object,
};
