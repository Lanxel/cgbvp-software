import React, {Component} from 'react';
import RecorderJS from 'recorder-js';

import { getAudioStream, exportBuffer } from '../../utils/audio';
import Button from "../CustomButtons/Button";
import KeyboardVoiceIcon from "@material-ui/icons/KeyboardVoice";
import StopIcon from "@material-ui/icons/Stop";
import {TextField} from "@material-ui/core";
import {primaryColor} from "../../styles/jss/nextjs-material-kit";
import {Bars, SpinningCircles} from 'react-loading-icons';
import {useTranscriber} from "../../hooks/api/transcription";

class Recorder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      stream: null,
      recording: false,
      recorder: null,
      audioResult: '',
      transcription: '',
    };
    this.startRecord = this.startRecord.bind(this);
    this.stopRecord = this.stopRecord.bind(this);
  }

  async componentDidMount() {
    let stream;
    let audioResult = '';
    let transcription = '';
    try {
      stream = await getAudioStream();
    } catch (error) {
      // Users browser doesn't support audio.
      // Add your handler here.
      console.log(error);
    }

    this.setState({ stream, audioResult, transcription });
  }

  startRecord() {
    const { stream } = this.state;

    const audioContext = new (window.AudioContext || window.webkitAudioContext)();
    const recorder = new RecorderJS(audioContext);
    recorder.init(stream);

    this.setState(
        {
          recorder,
          recording: true,
          audioResult: '',
          transcription: '',
        },
        () => {
          recorder.start();
        }
    );
  }

  async stopRecord() {
    const { recorder } = this.state;

    const { buffer } = await recorder.stop();
    const audio = exportBuffer(buffer[0]);
    // Process the audio here.
    // eslint-disable-next-line react/prop-types
    const { onRecord } = this.props;
    onRecord("Sending");
    const transcription = await useTranscriber(audio);
    onRecord("Finished");
    // eslint-disable-next-line react/prop-types
    const {onSubmit} = this.props;
    onSubmit(transcription.data.toString());
    this.setState({
      recording: false,
      audioResult: window.URL.createObjectURL(audio),
      transcription: transcription.data.toString(),
    });
  }

  render() {
    const {recording, stream, audioResult, transcription} = this.state;
    // Don't show record button if their browser doesn't support it.
    if (!stream) {
      return null;
    }
    // eslint-disable-next-line react/prop-types
    const {onSubmit, transcriptionStatus} = this.props;

    const transcript = transcription;
    return (
        <>
          { !recording && audioResult.length <= 0 && transcriptionStatus !=='Sending' &&
              <div>
                <Button justIcon round size="lg" onClick={() => this.startRecord()}
                        style={{backgroundColor: "#E7E8ED"}}><KeyboardVoiceIcon style={{color: "#C00812"}}/></Button>
                <div>Presione para grabar</div>
              </div>
          }
          { recording && transcriptionStatus !=='Sending' &&
              <div>
                <div>
                  <Bars fill='black' height="3em"/>
                </div>
                <Button justIcon round size="lg" onClick={() => this.stopRecord()} style={{backgroundColor: "#E7E8ED"}}>
                  <StopIcon style={{color: "#C00812"}}/></Button>
                <div>Presione el botón para traducir el audio</div>
              </div>
          }
          { transcriptionStatus ==='Sending' &&
              <div style={{justifyContent: "center"}}>
                <SpinningCircles fill={primaryColor} stroke={primaryColor} height="3em"/>
              </div>
          }
          { !recording && audioResult.length > 0 &&
              <div style={{marginTop: "1rem", display: "inherit", flexDirection: "column"}}>
                <audio controls src={audioResult}/>
                <TextField
                    id="filled-multiline-static"
                    label="Traducción"
                    multiline
                    rows={6}
                    defaultValue={transcript}
                    variant="filled"
                    color={"secondary"}
                    onChange={(event) => onSubmit(event.target.value)}
                />
                <Button color="transparent" style={{color:primaryColor}} onClick={() => this.startRecord()} onChange={(newValue) => onSubmit(newValue.target.value)}>Volver a grabar</Button>
              </div>
          }
        </>
    );
  }
}

export default Recorder;