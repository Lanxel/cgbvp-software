
const homeStyle = {
  content: {
    flexGrow: 1,
    height: "100vh",
  },
  menuButton: {
    "@media (min-width: 576px)": {
      display: "none",
    },
  },
};

export default homeStyle;