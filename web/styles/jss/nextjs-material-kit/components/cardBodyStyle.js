import customCheckboxRadioSwitch from "styles/jss/nextjs-material-kit/customCheckboxRadioSwitch";

const cardBodyStyle = {
  ...customCheckboxRadioSwitch,
  cardBodyEmergency: {
    padding: "0.4rem 1.5rem",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between"
  },
  cardBodyMessage: {
    padding: "1.25rem 1.25rem",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between"
  },
  cardBodyForm:{
    padding: "1.25rem 1.25rem",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
  },
  newEmergencyBody:{
    justifyContent: "space-between",
    textAlign:"center",
    minWidth: 600,
    '@media (max-width: 600px)':{
      minWidth: 200,
    },
  },
  userCardBody:{
    padding: "0.4rem 1.5rem",
    display: "flex",
    textAlign:"center",
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "center"
  },
  emergencyBody:{
    height: "330px",
    display: "flex",
    flexDirection:"column",
    justifyContent:"space-around",
    textAlign:"center",
  },
  title: {
    fontWeight: 700,
    fontSize: "18px",
  },
  contain: {
    fontSize: "14px",
    fontWeight: 400,
  },
  description:{
    fontSize: "14px",
    lineHeight: "initial",
  },
  messageBody:{
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start"
  },
  icon:{
    height: "14px",
    width: "14px",
    marginRight: "5px",
  },
  commentButton:{
    padding: "5px 0px 0px 0px",
    justifyContent: "initial",
    fontSize: "12px",
    width: "50%",
    color: "#404040"
  },
  formControl:{
    width: "inherit"
  },
  listContainer:{
    marginTop: "10px",
    display: "flex",
    position: "relative",
    justifyContent: "center",
  },
  elementList:{
    background: "#929698",
  },
  profileImageContainer:{
    marginTop: "25px",
    lineHeight: "1.5rem"
  },
  profileImage:{
    margin: "10px auto",
    width: "60px",
    height: "60px",
    borderRadius: "100%",
  },
  userBody:{
    display: "grid",
    maxWidth: "250px",
  },
  userFormBody:{
    display: "flex",
    flexDirection: "column",
    maxWidth: "100%",
    justifyContent: "center",
  },
  labelClass:{
    width: "300px",
    margin: "0 1rem"
  },
  flexContain:{
    display: "flex",
    justifyContent: "space-between",
    margin: "0.25rem 0",
  }

};

export default cardBodyStyle;
